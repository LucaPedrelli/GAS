// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayCueNotify_Static.h"
#include "GASPunchGameplayCue.generated.h"

class UAnimMontage;

/**
 * This is a custom static gameplay cue that we'll send with the damage event.
 * Check the gameplay cues section of the "GE_PunchDamage" blueprint
 */
UCLASS()
class GAS_API UGASPunchGameplayCue : public UGameplayCueNotify_Static
{
	GENERATED_BODY()

public:
	UGASPunchGameplayCue();

	virtual void HandleGameplayCue(AActor* MyTarget, EGameplayCueEvent::Type EventType, const FGameplayCueParameters& Parameters) override;

private:
	UPROPERTY()
	UAnimMontage* HitReactMontage;
};
