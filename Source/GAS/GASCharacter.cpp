// Copyright Epic Games, Inc. All Rights Reserved.

#include "GASCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "GASAbilitySystemComponent.h"
#include "GASAttributeSet.h"
#include "GASGameplayAbility.h"
#include <GameplayEffectTypes.h>
#include "Kismet/KismetSystemLibrary.h"
#include "AbilitySystemBlueprintLibrary.h"

//////////////////////////////////////////////////////////////////////////
// AGASCharacter

AGASCharacter::AGASCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	AbilitySystemComponent = CreateDefaultSubobject<UGASAbilitySystemComponent>(TEXT("AbilitySystemComp"));
	AbilitySystemComponent->SetIsReplicated(true);
	// Minimal and Mixed are recommended for multiplayer games, Full for singleplayer games.
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);

	Attributes = CreateDefaultSubobject<UGASAttributeSet>(TEXT("Attributes"));

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

class UAbilitySystemComponent* AGASCharacter::GetAbilitySystemComponent() const 
{
	return AbilitySystemComponent;
}

void AGASCharacter::InitializeAttributes() 
{
	if (!AbilitySystemComponent || !DefaultAttributeEffect)
		return;
	
	/** 
	 * Here is how to apply effects to an ability system component, i.e. how to change a character
	 * attributes:
	 * - FGameplayEffectContextHandle stores info about the context on which we're applying the
	 *   effect. Example: Character A shoots character B => Context: A is the instigator, B is the
	 *   target.
	 * - FGameplayEffectSpecHandle is needed because effects can have different properties (e.g.
	 *   level)
	 * - If the EffectSpecHandle is valid we can apply the changes to the attributes through our
	 *   effect
	*/
	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	/** 
	 * We're telling the system that we're using the default effect, at level 1, in the context we
	 * just created
	*/
	FGameplayEffectSpecHandle SpecHandle = AbilitySystemComponent->MakeOutgoingSpec(DefaultAttributeEffect, 1, EffectContext);

	if(SpecHandle.IsValid())
	{
		/**
		 * Applying the changes to our attributes through the effect. There is another function
		 * called ApplyGameplayEffectSpecToTarget used if target obj isn't this (e.g., this is
		 * shooting to somebody else).
		*/
		FActiveGameplayEffectHandle GEHandle = 
			AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
	}
}

void AGASCharacter::GiveAbilities() 
{
	// Here is how to give abilities to the character

	// 1. Checking if we're the server (the server should be the only one that gives abilities)
	if(!HasAuthority() || !AbilitySystemComponent)
		return;
	
	for (TSubclassOf<UGASGameplayAbility>& Ability : DefaultAbilities)
	{
		// 2. Give the ability to the player through an FGameplayAbilitySpec instance
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(
			Ability,	// Ability
			1,			// Level of the ability
			static_cast<int32>(Ability.GetDefaultObject()->AbilityInputID),	// Input ID
			this		// Source object
		));
	}
}

void AGASCharacter::PossessedBy(AController* NewController) 
{
	Super::PossessedBy(NewController);

	/** 
	 * InitAbiltyActorinfo initializes the GAS. PossessedBy is called on server.
	 * This function tells the GAS who the owner and the avatar of the AbilityComponent are.
	 * - The owner is the one that has the component attached to it. It is this class in our case,
	 *   but it could be an APlayerState subclass.
	 * - The avatar is the physical thing you can see in the world, it's always a subclass of
	 *   ACharacter.
	 * 
	 * In our case, this class is going to be both the owner and the avatar.
	*/
	AbilitySystemComponent->InitAbilityActorInfo(this, this);

	// Assigning default values to the attributes and default abilities (punch)
	InitializeAttributes();
	GiveAbilities();
}

void AGASCharacter::OnRep_PlayerState() 
{
	Super::OnRep_PlayerState();

	/** 
	 * InitAbilityActorInfo initializes the GAS. OnRep_PlayerState is called on client.
	*/
	AbilitySystemComponent->InitAbilityActorInfo(this, this);

	/** 
	 * We're still going to initialize attributes, but only the server gets to give abilities,
	 * the client doesn't need to do that.
	*/
	InitializeAttributes();
	if (AbilitySystemComponent && InputComponent)
	{
		/** 
		 * One thing the client has to do is bind the input to the abilities. 
		 * We give FGameplayAbilityInputBinds the name of our input enum, and magic will happen
		 * behind the scenes where it will look through and match our enum up to inputs.
		*/
		const FGameplayAbilityInputBinds Binds(
			FString("Confirm"),
			FString("Cancel"),
			FString("EGASAbilityInputID"),
			static_cast<int32>(EGASAbilityInputID::Confirm),
			static_cast<int32>(EGASAbilityInputID::Cancel)
		);
		AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, Binds);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGASCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGASCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGASCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGASCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AGASCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AGASCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AGASCharacter::OnResetVR);

	/** 
	 * The following code is exactly the same in OnRep_PlayerState(). It has been copied here
	 * because sometimes the AbilitySystemComponent will be valid but the input won't be and
	 * one function will get called before the other. By putting the code in both functions we're
	 * guaranteed that the ability activation will get bound properly
	*/
	if (AbilitySystemComponent && InputComponent)
	{
		const FGameplayAbilityInputBinds Binds(
			FString("Confirm"),
			FString("Cancel"),
			FString("EGASAbilityInputID"),
			static_cast<int32>(EGASAbilityInputID::Confirm),
			static_cast<int32>(EGASAbilityInputID::Cancel)
		);
		AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, Binds);
	}
}


void AGASCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AGASCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AGASCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AGASCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AGASCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AGASCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AGASCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AGASCharacter::HandlePunch() 
{
	// We check for hits in a sphere of radius 50cm around the right hand bone
	FVector HitSpherePos = GetMesh()->GetSocketLocation(FName(TEXT("hand_r")));
	float HitRadius = 50.f;

	// We can only hit things that have object type Pawn
	TArray <TEnumAsByte<EObjectTypeQuery>> CollisionFilters;
	CollisionFilters.Emplace(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

	// We want to ignore ourselves
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Emplace(this);

	// Where we store the actors we hit
	TArray<AActor*> OverlappedActors;

	UKismetSystemLibrary::SphereOverlapActors(
		GetWorld(),
		HitSpherePos,
		HitRadius,
		CollisionFilters,
		GetClass(),
		ActorsToIgnore,
		OverlappedActors
	);

	// The punch is sent only to the first actor hit
	if(!OverlappedActors.IsValidIndex(0)) // i.e. if we didn't hit
		return;
	
	FGameplayEventData Payload;
	Payload.Instigator = GetInstigator();
	Payload.Target = OverlappedActors[0];
	Payload.TargetData = 
		UAbilitySystemBlueprintLibrary::AbilityTargetDataFromActor(OverlappedActors[0]);
	
	// Tags are created via Project Settings, we just need to read them
	FGameplayTag PunchTag = FGameplayTag::RequestGameplayTag(FName(TEXT("Weapon.Hit")), true);

	/** 
	 * Sending ourselves a GameplayEvent with tag Weapon.Hit which will cause us to fire
	 * another GameplayEvent that damages the OverlappedActors[0] (see GASPunchAbility.cpp)
	*/
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(
		GetInstigator(),
		PunchTag,
		Payload
	);
	// AbilitySystemComponent->HandleGameplayEvent(PunchTag, &Payload);
}