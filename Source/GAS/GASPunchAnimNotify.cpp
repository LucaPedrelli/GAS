// Fill out your copyright notice in the Description page of Project Settings.


#include "GASPunchAnimNotify.h"
#include "GASCharacter.h"


UGASPunchAnimNotify::UGASPunchAnimNotify() 
{
    
}

void UGASPunchAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) 
{
    Super::Notify(MeshComp, Animation);
    if(AGASCharacter* PunchingCharacter = Cast<AGASCharacter>(MeshComp->GetOwner()))
        PunchingCharacter->HandlePunch();
}
