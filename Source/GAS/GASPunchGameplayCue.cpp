// Fill out your copyright notice in the Description page of Project Settings.


#include "GASPunchGameplayCue.h"
#include "Animation/AnimInstance.h"
#include "GASCharacter.h"


UGASPunchGameplayCue::UGASPunchGameplayCue() 
{
    /**
     * The GameplayCueTag is created via editor (Window>GameplayCue Editor), here we just retrieve
     * it.
     * GameplayCue's gameplay tags MUST start with the "GameplayCue." field.
    */
    GameplayCueTag = FGameplayTag::RequestGameplayTag(FName(TEXT("GameplayCue.TookDamage")), true);

    static ConstructorHelpers::FObjectFinder<UAnimMontage> HitReactMontageFinder(TEXT("AnimMontage'/Game/M_HitReact.M_HitReact'"));
	if (HitReactMontageFinder.Object)
		HitReactMontage = HitReactMontageFinder.Object;
}

void UGASPunchGameplayCue::HandleGameplayCue(AActor* MyTarget, EGameplayCueEvent::Type EventType, const FGameplayCueParameters& Parameters) 
{
    Super::HandleGameplayCue(MyTarget, EventType, Parameters);

    if (EventType == EGameplayCueEvent::Executed)
        if (AGASCharacter* PunchTarget = Cast<AGASCharacter>(MyTarget))
            if (HitReactMontage)
                PunchTarget->PlayAnimMontage(HitReactMontage);
}
