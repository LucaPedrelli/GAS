// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GASGameplayAbility.h"
#include "GASPunchAbility.generated.h"

class UAnimMontage;
class UAbilityTask_WaitGameplayEvent;
class UGameplayEffect;

UCLASS()
class GAS_API UGASPunchAbility : public UGASGameplayAbility
{
	GENERATED_BODY()

public:
	UGASPunchAbility();

protected:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

private:
	UPROPERTY()
	UAnimMontage* PunchMontage;

	UAbilityTask_WaitGameplayEvent* PunchHitEvent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS", meta=(AllowPrivateAccess="true"))
	TSubclassOf<UGameplayEffect> DamageEffectClass;

	// Called by FOnMontageEnded delegate, which requirese these two params
	UFUNCTION()
	void EndAbilityFromAnimation(UAnimMontage* AnimMontage, bool bInterrupted);

	// Called by delegate of type FWaitGameplayEventDelegate, which requires this param
	UFUNCTION()
	void ApplyPunchDamage(FGameplayEventData Payload);
};
