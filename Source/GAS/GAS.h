// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

// To bind ability to input
UENUM(BlueprintType)
enum class EGASAbilityInputID : uint8
{
    None,
    Confirm,
    Cancel,
    Punch
};
