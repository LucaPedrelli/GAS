// Fill out your copyright notice in the Description page of Project Settings.


#include "GASPunchAbility.h"
#include "Animation/AnimInstance.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"

UGASPunchAbility::UGASPunchAbility() 
{
    static ConstructorHelpers::FObjectFinder<UAnimMontage> PunchMontageFinder(TEXT("AnimMontage'/Game/M_Attack.M_Attack'"));
	if (PunchMontageFinder.Object)
		PunchMontage = PunchMontageFinder.Object;
}

void UGASPunchAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) 
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    /** 
     * 1. Play the attack animation
     *      - A UAnimNotify is fired during such animation, calling the HandlePunch function on the
     *        skill owner
    */
    if (UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance())
    {
        if (!PunchMontage)
            return;
        // Montage_Play starts the montage
        const float MontageLength = AnimInstance->Montage_Play(PunchMontage);
        if (MontageLength <= 0.f) // i.e. if not played successfully
            return;

        /** 
         * Here we set the montage delegates so that the ability can end when the animation ends
         * or blends out. 
         */
        FOnMontageEnded CompletedPunchDelegate;
        CompletedPunchDelegate.BindUObject(
            this, 
            &UGASPunchAbility::EndAbilityFromAnimation
        );
        AnimInstance->Montage_SetEndDelegate(CompletedPunchDelegate, PunchMontage);
        
        FOnMontageBlendingOutStarted BlendingOutPunchDelegate;
        BlendingOutPunchDelegate.BindUObject(
            this, 
            &UGASPunchAbility::EndAbilityFromAnimation
        );
        AnimInstance->Montage_SetBlendingOutDelegate(BlendingOutPunchDelegate, PunchMontage);
    }

    /** 
     * 2. Wait until the specified gameplay tag event is triggered on the owner of this ability 
     *      - The event is triggered if AGASCharacter::HandlePunch() detects a pawn near the right
     *        hand bone during the punch animation.
    */
    PunchHitEvent = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(
        this,
        FGameplayTag::RequestGameplayTag(FName(TEXT("Weapon.Hit")), true),
        nullptr,
        true,
        true
    );
    /**
     * 3. When the event specified by the tag triggers we apply damage
    */
    PunchHitEvent->EventReceived.AddDynamic(this, &UGASPunchAbility::ApplyPunchDamage);
    PunchHitEvent->Activate(); // Starts the async task
}

void UGASPunchAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) 
{
    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

    if (PunchHitEvent) // Cleaning up the async event
        PunchHitEvent->EndTask();
}

void UGASPunchAbility::EndAbilityFromAnimation(UAnimMontage* AnimMontage, bool bInterrupted) 
{
    /** 
     * This function has been made by taking inspiration from UGameplayAbility::K2_EndAbility().
     * Source: GameplayAbility.cpp
    */
    check(CurrentActorInfo);

	bool bReplicateEndAbility = true;
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, bReplicateEndAbility, bInterrupted);
}

void UGASPunchAbility::ApplyPunchDamage(FGameplayEventData Payload) 
{
    if (!DamageEffectClass)
        return;

    BP_ApplyGameplayEffectToTarget(
        Payload.TargetData,
        DamageEffectClass
    );
}
