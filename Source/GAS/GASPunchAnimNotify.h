// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "GASPunchAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class GAS_API UGASPunchAnimNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	UGASPunchAnimNotify();

	/**
	 * Fired during an UAnimMontage when designers want to.
	 */
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
