// Fill out your copyright notice in the Description page of Project Settings.


#include "GASAttributeSet.h"
#include "Net/UnrealNetwork.h"



UGASAttributeSet::UGASAttributeSet() 
{
    
}

void UGASAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const 
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    /**
     * Usually OnRep methods are called only when the value of the attribute changes, i.e. when
     * the local value is different from the value being repped down from the Server.
     * 
     * With REPNOTIFY_Always we tell the system that we want to call the OnReps even if the local
     * value is already equal to the one being repped down.
     */
    DOREPLIFETIME_CONDITION_NOTIFY(UGASAttributeSet, Health, COND_None, REPNOTIFY_Always);
    DOREPLIFETIME_CONDITION_NOTIFY(UGASAttributeSet, Stamina, COND_None, REPNOTIFY_Always);
    DOREPLIFETIME_CONDITION_NOTIFY(UGASAttributeSet, AttackPower, COND_None, REPNOTIFY_Always);
}

void UGASAttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth) 
{
    /**
     * GAS requires that we tell it whenever an attribute changes, it's part of the system.
     * To do so, we use the GAMEPLAYATTRIBUTE_REPNOTIFY macro.
     */
    GAMEPLAYATTRIBUTE_REPNOTIFY(UGASAttributeSet, Health, OldHealth);
}

void UGASAttributeSet::OnRep_Stamina(const FGameplayAttributeData& OldStamina) 
{
    GAMEPLAYATTRIBUTE_REPNOTIFY(UGASAttributeSet, Stamina, OldStamina);
}

void UGASAttributeSet::OnRep_AttackPower(const FGameplayAttributeData& OldAttackPower) 
{
    GAMEPLAYATTRIBUTE_REPNOTIFY(UGASAttributeSet, AttackPower, OldAttackPower);
}
