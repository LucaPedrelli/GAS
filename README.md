# GAS

Repo made to familiarize with Unreal4's Gameplay Ability System (GAS) before implementing it in other projects.

## Punch Ability

The punch ability has been made following [Reuben's video](https://www.youtube.com/watch?v=Yub52f4ZUU0&t=1871s&ab_channel=ReubenWard) on GAS but using only C++ for the logic: there isn't a single BP block in the event graphs, BPs are used only for setting values.

### Links and documentation

- [Here](https://www.youtube.com/watch?v=Yub52f4ZUU0&ab_channel=ReubenWard) is the YouTube video I'll follow to make the "punch" ability. The project files I am starting with are the ones provided in the video description.
- [Here](https://github.com/tranek/GASDocumentation#intro) is a sample project with a very detailed documentation on GAS. Maybe I'll try to recreate the "Meteor" skill in C++.
- [GameplayAbility explanation](https://docs.unrealengine.com/en-US/InteractiveExperiences/GameplayAbilitySystem/GameplayAbility/index.html)
- [Playing montages from C++](https://answers.unrealengine.com/questions/889542/play-montage-in-c-with-onblendout-oninterrupted-et.html)
- [Adding delegates to the montage to execute funcitons](https://forums.unrealengine.com/development-discussion/c-gameplay-programming/1592904-animation-montages-on-interrupt-on-blend-out-on-completed-in-c)
- [GameplayTags](https://docs.unrealengine.com/en-US/ProgrammingAndScripting/Tags/index.html) creation and management
- [Delegates explanation](https://unreal.gg-labs.com/wiki-archives/macros-and-data-types/delegates-in-ue4-raw-c++-and-bp-exposed)